﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Persona
    {
        public Persona(string nombre, string apellido, string documentoRuta)
        {
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.DocumentoRuta = documentoRuta;
        }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string DocumentoRuta { get; set; }
    }
}
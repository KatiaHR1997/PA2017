﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Empleado : Persona
    {
        public Empleado (string nombre, string apellido, string documentoRuta) : base (nombre, apellido, documentoRuta)
        {

        }
        public int IdTipoContrato { get; set; }
        public double Sueldo { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Grabadora : Articulo
    {
        public Grabadora(string color, string marca, string tamaño) : base (color, marca, tamaño)
        {

        }
        public int memoria { get; set; }
        public string bateria { get; set; }
        public string formato { get; set; }
    }
}
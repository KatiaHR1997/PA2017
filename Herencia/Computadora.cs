﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Computadora : Articulo
    {
        public Computadora(string color, string marca, string tamaño) : base (color, marca, tamaño)
        {

        }
        public string Procesador { get; set; }
        public int RAM { get; set; }
        public int sistemOperative { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Celular : Articulo
    {
        public Celular(string color, string marca, string tamaño) : base (color, marca, tamaño)
        {

        }
        public string modelo { get; set; }
    }
}
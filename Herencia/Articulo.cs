﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Articulo
    {
        public Articulo(string color, string marca, string tamaño)
        {
            this.Color = color;
            this.Marca = marca;
            this.Tamaño = tamaño;
        }
        public string Color { get; set; }
        public string Marca { get; set; }
        public string Tamaño { get; set; }
    }
}
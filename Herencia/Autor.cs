﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Autor : Blog
    {
        public Autor (string nombre, string apellido) : base (nombre, apellido)
        {

        }
        public string titulo { get; set; }
        public string FechaPublicacion { get; set; }
        public string Contenido { get; set; }
    }
}
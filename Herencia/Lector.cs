﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Lector : Blog
    {
        public Lector (string nombre, string apellido) : base(nombre, apellido)
        {

        }
        public string email { get; set; }
        public string comentario { get; set; }
    }
}
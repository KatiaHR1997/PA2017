﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Cliente : Persona
    {
        public Cliente (string nombre, string apellido, string documentoRuta) : base (nombre, apellido, documentoRuta)
        {

        }
        public int IdCategoria { get; set; }
        public int Codigo { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Registro Empleado */
            Empleado empleado1 = new Empleado("Katia", "Hernandez", "https://www.facebook.com/profile.php?id=100002164600047");
            empleado1.IdTipoContrato = 1;
            empleado1.Sueldo = 2654.23;
            Console.WriteLine(">>>>> E M P L E A D O <<<<<");
            Console.WriteLine("Nombre y apellido: " + empleado1.Nombre + " " + empleado1.Apellido);
            Console.WriteLine("Ruta: " + empleado1.DocumentoRuta);
            Console.WriteLine("Id tipo de contrato: " + empleado1.IdTipoContrato);
            Console.WriteLine("Sueldo: " + empleado1.Sueldo);

            /* Registro Cliente */
            Cliente cliente1 = new Cliente("Cristhiam", "Bermúdez", "https://www.facebook.com/cristian.bermudez.750331");
            cliente1.IdCategoria = 1;
            cliente1.Codigo = 1;
            Console.WriteLine("\n>>>>> C L I E N T E <<<<<");
            Console.WriteLine("Nombre y apellido: " + empleado1.Nombre + " " + empleado1.Apellido);
            Console.WriteLine("Ruta: " + cliente1.DocumentoRuta);
            Console.WriteLine("Id de categoría: " + cliente1.IdCategoria);
            Console.WriteLine("Código: " + cliente1.Codigo);

            /* Registro Autor */
            Autor autor1 = new Autor("Alisson", "Aguilar");
            autor1.titulo = "Ingeniería del software asistida por computadoras";
            autor1.FechaPublicacion = "12/05/2018";
            autor1.Contenido = "Las herramientas CASE alcanzaron su techo a principios de los años 90." +
                                "En la época en la que IBM había conseguido una alianza con la empresa de software AD/Cycle para trabajar con sus" +
                                "mainframes, estos dos gigantes trabajaban con herramientas CASE que abarcaban todo el ciclo de vida del software." +
                                "Pero poco a poco los mainframes han ido siendo menos utilizados y actualmente el mercado de las Big CASE ha" +
                                "muerto completamente abriendo el mercado de diversas herramientas más específicas para cada fase del ciclo de" +
                                "vida del software.";
            Console.WriteLine("\n>>>>> A U T O R <<<<<");
            Console.WriteLine("Nombre y apellido: " + autor1.Nombre + " " + autor1.Apellido);
            Console.WriteLine("Título del blog: " + autor1.titulo);
            Console.WriteLine("Fecha de publicación: " + autor1.FechaPublicacion);
            Console.WriteLine("\n--- Contenido del blog ---");
            Console.WriteLine(autor1.Contenido);

            /* Registro Lector */
            Lector lector1 = new Lector("Alicia", "Diaz");
            lector1.email = "gAliciaDiaz@gmail.com";
            lector1.comentario = "Muy buen blog, es muy útil. Gracias por aclarar nuestras dudas.";
            Console.WriteLine("\n>>>>> L E C T O R <<<<<");
            Console.WriteLine("Nombre y apellido: " + lector1.Nombre + " " + lector1.Apellido);
            Console.WriteLine("Email: " + lector1.email);
            Console.WriteLine("Comentario: " + lector1.comentario);

            /* Registro Grabadora */
            Grabadora grabadora1 = new Grabadora("Gris", "OLYMPUS", "38,5 x 115,2 x 21,3 mm");
            grabadora1.memoria = 4;
            grabadora1.bateria = "AAA x 2";
            grabadora1.formato = "MP3";
            Console.WriteLine("\n>>>>> G R A B A D O R A <<<<<");
            Console.WriteLine("Color: " + grabadora1.Color);
            Console.WriteLine("Marca: " + grabadora1.Marca);
            Console.WriteLine("Tamaño: " + grabadora1.Tamaño);
            Console.WriteLine("Tamaño de memoria: " + grabadora1.memoria);
            Console.WriteLine("Tipo de batería: " + grabadora1.bateria);
            Console.WriteLine("Formato: " + grabadora1.formato);

            /* Registro Computadora */
            Computadora computadora1 = new Computadora("Negro", "HP", "365 x 4586");
            computadora1.Procesador = "Intel Core i5-3470 CPU 3.20GHz";
            computadora1.RAM = 8;
            computadora1.sistemOperative = 64;
            Console.WriteLine("\n>>>>> C O M P U T A D O R A <<<<<");
            Console.WriteLine("Color: " + computadora1.Color);
            Console.WriteLine("Marca: " + computadora1.Marca);
            Console.WriteLine("Tamaño: " + computadora1.Tamaño);
            Console.WriteLine("Procesador: " + computadora1.Procesador);
            Console.WriteLine("RAM: " + computadora1.RAM);
            Console.WriteLine("Sistema Operativo: " + computadora1.sistemOperative);

            Console.ReadKey(); // Permite que la consola no se cierre.
        }
    }
}